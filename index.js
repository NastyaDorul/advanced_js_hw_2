function Burger(size, stuffing, dressing) {
  this.checkASize = () => size;
  this.findStuffing = () => stuffing;
  this.findDressing = () => dressing;

  try {
    try {
      if (!this.burgerSize[size]) {
        const e = new Error("cannot create new burger with desired size");
        e.name = "BurgerSizeError";
        throw e;
      }
      if (!this.stuffingTypes[stuffing]) {
        const e = new Error("cannot create new burger with desired stuffing");
        e.name = "StuffingError";
        throw e;
      }
      if (!this.dressingTypes[dressing]) {
        const e = new Error("cannot create new burger with desired dressing");
        e.name = "DressingError";
        throw e;
      }
      this.size = size;
      this.stuffing = stuffing;
      this.dressing = dressing;
    } catch (err) {
      if (err.name == "BurgerSizeError") {
        console.log("sizeErr: " + err);
      }
      if (err.name == "StuffingError") {
        console.log("stuffErr: " + err);
      }
      if (err.name == "DressingError") {
        console.log("dressErr: " + err);
      } else {
        throw err;
      }
    }
  } catch (err) {
    console.log("otherErr: " + err);
  }
}
Burger.prototype.burgerSize = {
  small: {
    price: 50,
    calories: 20
  },
  large: {
    price: 100,
    calories: 40
  }
};

Burger.prototype.stuffingTypes = {
  cheese: {
    calories: 20,
    price: 10
  },
  salad: {
    calories: 5,
    price: 20
  },
  potato: {
    calories: 10,
    price: 15
  }
};
Burger.prototype.dressingTypes = {
  sauce: {
    calories: 0,
    price: 15
  },
  mayo: {
    calories: 5,
    price: 20
  }
};

//------------------------methods-to-change-sizes-------------------------------
Burger.prototype.makeSmallSize = function() {
  console.log(`Previous choice -  ${this.checkASize()} size, changed to small`);
  this.burgerSize[this.checkASize()]["calories"] = 0;
  this.burgerSize[this.checkASize()]["price"] = 0;
  return (this.size = "small");
};
Burger.prototype.makeLargeSize = function() {
  console.log(`Previous choice -  ${this.checkASize()} size, changed to large`);
  this.burgerSize[this.checkASize()]["calories"] = 0;
  this.burgerSize[this.checkASize()]["price"] = 0;
  return (this.burgerSize[this.checkASize()] = this.burgerSize.large);
};
Burger.prototype.seeCurrentSize = function() {
  return this.size;
};
//------------------------methods-to-change---dressing--------------------------
Burger.prototype.sauceDressing = function() {
  console.log(`Will add sauce dressing`);
  this.dressingTypes[this.findDressing()] = this.dressingTypes.sauce;
  return (this.dressing = "sauce");
};
Burger.prototype.mayoDressing = function() {
  console.log(`Will add mayo dressing`);
  this.dressingTypes[this.findDressing()] = this.dressingTypes.mayo;
  return (this.dressing = "mayo");
};
Burger.prototype.noDressing = function() {
  console.log(`Will take away any dressing`);
  this.dressingTypes[this.findDressing()]["price"] = 0;
  this.dressingTypes[this.findDressing()]["calories"] = 0;
  return (this.dressing = "no dressing");
};
Burger.prototype.seeCurrentDressing = function() {
  return this.dressing;
};
//------------------------methods-to-change--stuffing-----------------------------
Burger.prototype.cheeseStuffing = function() {
  if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.cheese) {
    return "You should choose another stuffing";
  }
  this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.cheese;
  return (this.stuffing = "cheese");
};

Burger.prototype.potatoStuffing = function() {
  if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.potato) {
    return "You should choose another stuffing";
  }
  this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.potato;
  return (this.stuffing = "potato");
};

Burger.prototype.saladStuffing = function() {
  if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.salad) {
    return "You should choose another stuffing";
  }
  this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.salad;
  return (this.stuffing = "salad");
};
/*Burger.prototype.additionalStuffing = function() {
    console.log(`You already have ${this.stuffing} `); //work with this part
    return this.stuffingTypes[this.findStuffing()];
  };*/
Burger.prototype.seeCurrentStuffing = function() {
  return this.stuffing;
};
//---------------------calculate--burger--price--------------------------------
Burger.prototype.price = function() {
  if (this.dressingTypes[this.findDressing()] !== undefined) {
    return (
      this.burgerSize[this.checkASize()]["price"] +
      this.dressingTypes[this.findDressing()]["price"] +
      this.stuffingTypes[this.findStuffing()]["price"]
    );
  }
  return (
    this.burgerSize[this.checkASize()]["price"] +
    this.stuffingTypes[this.findStuffing()]["price"]
  );
};
//---------------------calculate--burger--calories-----------------------------
Burger.prototype.fat = function() {
  if (this.dressingTypes[this.findDressing()] !== undefined) {
    return (
      this.burgerSize[this.checkASize()]["calories"] +
      this.dressingTypes[this.findDressing()]["calories"] +
      this.stuffingTypes[this.findStuffing()]["calories"]
    );
  }
  return (
    this.burgerSize[this.checkASize()]["calories"] +
    this.stuffingTypes[this.findStuffing()]["calories"]
  );
};

//-------------------------testing---------------------------------------------
let strangeBurger = new Burger("small", "mushrom", "tai");

console.log(Burger.prototype.stuffingTypes);
console.log(Burger.prototype.burgerSize);
console.log(Burger.prototype.dressingTypes);

let smallBurger = new Burger("small", "salad", "mayo");
let largeBurger = new Burger("large", "cheese", "sauce");
let noSauce = new Burger("small", "potato");

console.log(noSauce.fat());
console.log(noSauce.price());

console.log(largeBurger.findDressing());

console.log(largeBurger.checkASize());

console.log(largeBurger.makeSmallSize());

console.log(smallBurger.cheeseStuffing());
console.log(smallBurger.potatoStuffing());
console.log(smallBurger.noDressing());
console.log(smallBurger);
console.log(smallBurger.price());
console.log(largeBurger.fat());
console.log(smallBurger.seeCurrentStuffing());

console.log(smallBurger.potatoStuffing());

console.log(smallBurger.cheeseStuffing());
console.log(smallBurger.seeCurrentStuffing());

console.log(smallBurger.potatoStuffing());
console.log(smallBurger.seeCurrentStuffing());

console.log(largeBurger.saladStuffing());
console.log(largeBurger.seeCurrentStuffing());
